import json
import pytest
from flask import Flask, request
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_index_page(client):
    response = client.get('/')
    assert response.status_code == 200
    # assert b"Option A" in response.data
    # assert b"Option B" in response.data

# def test_vote_submission(client):
#     response = client.post('/', json=dict(vote='Cats'))
#     assert response.status_code == 500
#     # assert b"Thanks for voting!" in response.data

# def test_cookie_generation(client):
#     response = client.get('/')
#     voter_id = client.cookie_jar._cookies['localhost.local']['/']['voter_id'].value
#     assert voter_id

