import pytest
from flask import Flask
from redis import Redis


# @pytest.fixture
# def client():
#     app.testing = True
#     return app.test_client()


def add(a, b):
    return a + b


def test_add():
    assert add(1, 2) == 3
    assert add(-1, -2) == -3
    assert add(0, 0) == 0